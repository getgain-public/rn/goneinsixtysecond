import styled from '@emotion/native';
import Animated from 'react-native-reanimated';

export const AnimatedShadowOpacity = styled(Animated.View)`
  background-color: black;
`;
