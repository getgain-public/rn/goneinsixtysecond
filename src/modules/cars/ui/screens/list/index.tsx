import { dispatchAction } from '@src/store/helpers';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loadCars, loadNextCars } from '../../../store/actions';
import {
  carsStoreAllCarsSelector,
  carsStoreIsLoadingSelector,
} from '../../../store/selectors';
import * as S from './styles';
import CarsComponentsList from '../../components/cars-list';
import CarsComponentsDetail from '../../components/car-detail';
import { useBottomSheet } from '@src/modules/ud-ui/hooks/useBottomSheet';
import UDBottomSheet from '@src/modules/ud-ui/ud-bottom-sheet';
import { Car } from '@src/modules/cars/domain/interfaces/Car';

type Props = {};

const CarsScreenListScreen = (props: Props) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(carsStoreIsLoadingSelector);
  const cars = useSelector(carsStoreAllCarsSelector);
  const loadCarsAction = dispatchAction(dispatch, loadCars);
  const loadMoreAction = dispatchAction(dispatch, loadNextCars);
  useEffect(() => {
    loadCarsAction();
  }, []);
  const { ref, open } = useBottomSheet();
  const [selectedCar, setSelectedCar] = useState<Car | undefined>(undefined);
  const onShowCarDetail = useCallback(
    (car: Car) => {
      setSelectedCar(car);
      open();
    },
    [open, setSelectedCar],
  );
  return (
    <S.Container>
      <S.HeaderContainer>
        <S.Header style={{ letterSpacing: 1 }}>Gone In Sixty Seconds</S.Header>
      </S.HeaderContainer>
      <S.ContentContainer>
        <CarsComponentsList
          cars={cars}
          isLoading={isLoading}
          showCarDetail={onShowCarDetail}
          loadMore={loadMoreAction}
        />
      </S.ContentContainer>
      <UDBottomSheet sheetRef={ref} disableScrollIfPossible={true}>
        {selectedCar ? <CarsComponentsDetail car={selectedCar} /> : <></>}
      </UDBottomSheet>
    </S.Container>
  );
};

export default CarsScreenListScreen;
