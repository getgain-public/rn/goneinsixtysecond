import styled from '@emotion/native';

export const InfoFieldContainer = styled.View`
  flex-direction: row;
  margin-bottom: 4px;
  align-items: flex-end;
  flex-wrap: wrap;
  flex-shrink: 1;
`;

export const InfoFieldDescription = styled.Text`
  font-size: 18px;
  font-weight: 400;
  color: #333333;
`;

export const InfoFieldValue = styled.Text`
  margin-left: auto;
  font-size: 16px;
  font-weight: 500;
  color: black;
`;
