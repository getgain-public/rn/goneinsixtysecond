import React from 'react';
import * as S from './styles';

type Props = {
  description: string;
  value: string | number;
};

const CarsComponentsInfoField = (props: Props) => {
  const { description, value } = props;
  return (
    <S.InfoFieldContainer>
      <S.InfoFieldDescription>{description}:</S.InfoFieldDescription>
      <S.InfoFieldValue>{value}</S.InfoFieldValue>
    </S.InfoFieldContainer>
  );
};

export default CarsComponentsInfoField;
