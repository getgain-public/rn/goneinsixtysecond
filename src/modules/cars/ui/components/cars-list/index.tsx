import { Car } from '@src/modules/cars/domain/interfaces/Car';
import React, { useCallback } from 'react';
import { FlatList } from 'react-native';
import CarsComponentsListItem from './item';

type Props = {
  cars: Car[];
  isLoading: boolean;
  showCarDetail: (car: Car) => any;
  loadMore: () => any;
};

const CarsComponentsList = (props: Props) => {
  const { showCarDetail, cars, isLoading, loadMore } = props;

  const renderItem = useCallback(
    ({ item }) => {
      return <CarsComponentsListItem car={item} showDetail={showCarDetail} />;
    },
    [showCarDetail],
  );
  const keyExtractor = useCallback(item => {
    return `${item.id}`;
  }, []);
  return (
    <FlatList
      data={cars}
      renderItem={renderItem}
      refreshing={isLoading}
      keyExtractor={keyExtractor}
      scrollEnabled={true}
      onEndReached={loadMore}
      showsVerticalScrollIndicator={false}
    />
  );
};

export default CarsComponentsList;
