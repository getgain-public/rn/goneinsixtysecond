import styled from '@emotion/native';

export const CarListCard = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  margin-top: 10px;
  padding: 20px;
  background-color: ${({ theme }) => theme.colors.light};
  border-radius: 8px;
  overflow: hidden;
  border: 1px solid black;
`;

export const CarPhoto = styled.Image`
  border-radius: 4px;
  height: 100px;
  width: 100%;
  margin-bottom: 10px;
`;

export const CarPlate = styled.Text`
  border-radius: 4px;
  border: 2px solid black;
  padding: 5px;
  background-color: white;
  font-size: 20px;
  letter-spacing: 5px;
  color: black;
  text-transform: uppercase;
  font-weight: 600;
`;

export const InfoContainer = styled.View`
  padding-left: 10px;
  flex-wrap: wrap;
  flex-shrink: 1;
`;
