import React from 'react';
import { Car } from '@src/modules/cars/domain/interfaces/Car';
import * as S from './styles';

type Props = {
  locations: Car['locations'];
};

const CarsComponentsCarLocations = (props: Props) => {
  return (
    <S.Container>
      <S.LocationsLabel>Frequent locations:</S.LocationsLabel>
      <S.LocationsContainer>
        {props.locations.map(location => (
          <S.Location key={location}>- {location}</S.Location>
        ))}
      </S.LocationsContainer>
    </S.Container>
  );
};

export default CarsComponentsCarLocations;
