import styled from '@emotion/native';

export const Container = styled.View``;

export const LocationsContainer = styled.View`
  flex-direction: column;
  padding: 5px;
`;

export const LocationsLabel = styled.Text`
  font-size: 18px;
  font-weight: 400;
  color: #333333;
`;

export const Location = styled.Text`
  margin-bottom: 4px;
`;
