import styled from '@emotion/native';

export const DetailContainer = styled.View`
  background-color: transparent;
  min-height: 100%;
`;

export const DetailImage = styled.Image`
  width: 100%;
  height: 200px;
`;

export const CarPlate = styled.Text`
  border-radius: 4px;
  border: 2px solid black;
  padding: 5px;
  background-color: white;
  font-size: 20px;
  text-align: center;
  letter-spacing: 5px;
  color: black;
  text-transform: uppercase;
  font-weight: 600;
`;

export const InfoContainer = styled.View`
  padding: 20px;
`;

export const ButtonContainer = styled.TouchableOpacity`
  border-radius: 8px;
  margin-horizontal: 20px;
  background-color: black;
  padding-vertical: 20px;
`;

export const ButtonText = styled.Text`
  color: white;
  font-size: 24px;
  text-align: center;
`;
