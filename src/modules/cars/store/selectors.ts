import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '@src/store/store';
import { CarsStore } from '.';
import carsEntitiesAdapter from './adapters';

export const carsStoreSelector = (store: RootState): CarsStore => store.cars;

export const carsStoreAllCarsSelector = createSelector(
  carsStoreSelector,
  state => carsEntitiesAdapter.getSelectors().selectAll(state),
);

export const carsStoreAllItemsNumber = createSelector(
  carsStoreAllCarsSelector,
  cars => cars.length,
);

export const carsStoreIsLoadingSelector = createSelector(
  carsStoreSelector,
  state => state.isLoading,
);

export const carsStorePageSelector = createSelector(
  carsStoreSelector,
  state => state.page,
);

export const carsStoreCarByIdSelector = (id: number) =>
  createSelector(carsStoreSelector, state =>
    carsEntitiesAdapter.getSelectors().selectById(state, id),
  );
