import {createEntityAdapter} from '@reduxjs/toolkit';
import {Car} from '../domain/interfaces/Car';

const carsEntitiesAdapter = createEntityAdapter<Car>();
export default carsEntitiesAdapter;
