import { createAsyncThunk } from '@reduxjs/toolkit';
import { RootState } from '@src/store/store';
import { Car } from '../domain/interfaces/Car';
import carsRepository from '../domain/repositories/CarsRepository';
import { carsStorePageSelector, carsStoreAllItemsNumber } from './selectors';

const mockLocations = [
  '943 South Kenwood Ave Parking',
  'Eagle Creek Greenway, Indianapolis',
  '2210 Kentucky Ave, Indianapolis, IN 46221',
  '601 N Arlington Ave, Indianapolis',
];

const PREFIX = 'cars';

export const loadCars = createAsyncThunk<Car[], void>(
  `${PREFIX}/loadCars`,
  async () => {
    const response = await carsRepository.loadCars();
    return response.map(car => ({ ...car, locations: mockLocations }));
  },
);

export const loadNextCars = createAsyncThunk<
  { items: Car[]; page: number },
  void
>(`${PREFIX}/loadNextCars`, async (_, { getState }) => {
  const currentPage = carsStorePageSelector(getState() as RootState);
  const carsCount = carsStoreAllItemsNumber(getState() as RootState);
  const cars = await carsRepository.loadCars();
  const extended = cars.reduce(
    (acc, car) => {
      acc.items.push({ ...car, locations: mockLocations, id: acc.id });
      acc.id = acc.id + 1;
      return acc;
    },
    { items: [] as Car[], id: carsCount + 1 },
  );
  return {
    items: extended.items,
    page: currentPage + 1,
  };
});
